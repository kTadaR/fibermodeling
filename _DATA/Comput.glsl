﻿#version 430

//#extension GL_ARB_compute_variable_group_size : enable

//layout( local_size_variable ) in;
  layout( local_size_x = 10,
          local_size_y = 10,
          local_size_z =  1 ) in;

////////////////////////////////////////////////////////////////////////////////

  ivec3 _WorkGrupsN = ivec3( gl_NumWorkGroups );

//ivec3 _WorkItemsN = ivec3( gl_LocalGroupSizeARB );
  ivec3 _WorkItemsN = ivec3( gl_WorkGroupSize     );

  ivec3 _WorksN     = _WorkGrupsN * _WorkItemsN;

  ivec3 _WorkID     = ivec3( gl_GlobalInvocationID );

//############################################################################## ■

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【定数】

const float Pi         = 3.141592653589793;
const float Pi2        = Pi * 2;
const float P2i        = Pi / 2;
const float FLOAT_MAX  = 3.402823e+38;
const float FLOAT_EPS  = 1.1920928955078125E-7;
const float FLOAT_EPS1 = FLOAT_EPS * 1E1;
const float FLOAT_EPS2 = FLOAT_EPS * 1E2;

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【型】

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TdFloat

struct TdFloat
{
  float o;
  float d;
};

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TdVec3

struct TdVec3
{
  TdFloat x;
  TdFloat y;
  TdFloat z;
};

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【ルーチン】

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&（一般）

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Pow2

float Pow2( in float X )
{
  return X * X;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% length2

float length2( in vec3 V )
{
  return Pow2( V.x ) + Pow2( V.y ) + Pow2( V.z );
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% MinI

int MinI( in vec3 V )
{
  if ( V.x <= V.y ) {
    if ( V.x <= V.z ) return 0; else return 2;
  } else {
    if ( V.y <= V.z ) return 1; else return 2;
  }
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% MaxI

int MaxI( in vec3 V )
{
  if ( V.x >= V.y ) {
    if ( V.x >= V.z ) return 0; else return 2;
  } else {
    if ( V.y >= V.z ) return 1; else return 2;
  }
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Rand

uvec4 _RandSeed;

uint rotl( in uint x, in int k )
{
  return ( x << k ) | ( x >> ( 32 - k ) );
}

float Rand()
{
  const uint Result = rotl( _RandSeed[ 0 ] * 5, 7 ) * 9;

  const uint t = _RandSeed[ 1 ] << 9;

  _RandSeed[ 2 ] ^= _RandSeed[ 0 ];
  _RandSeed[ 3 ] ^= _RandSeed[ 1 ];
  _RandSeed[ 1 ] ^= _RandSeed[ 2 ];
  _RandSeed[ 0 ] ^= _RandSeed[ 3 ];

  _RandSeed[ 2 ] ^= t;

  _RandSeed[ 3 ] = rotl( _RandSeed[ 3 ], 11 );

  return uintBitsToFloat( Result & 0x007FFFFFu | 0x3F800000u ) - 1;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RandBS2

float RandBS2()
{
  return Rand() - Rand();
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RandBS4

float RandBS4()
{
  return RandBS2() + RandBS2();
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RandCirc

vec2 RandCirc()
{
  vec2 Result;
  float T, R;

  T = Pi2 * Rand();
  R = sqrt( Rand() );

  Result.x = R * cos( T );
  Result.y = R * sin( T );

  return Result;
}

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&（演算子）

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TdFloat

TdFloat Add( TdFloat A_, TdFloat B_ )
{
  TdFloat Result;

  Result.o = A_.o + B_.o;
  Result.d = A_.d + B_.d;

  return Result;
}

TdFloat Add( float A_, TdFloat B_ )
{
  TdFloat Result;

  Result.o = A_ + B_.o;
  Result.d =      B_.d;

  return Result;
}

TdFloat Add( TdFloat A_, float B_ )
{
  TdFloat Result;

  Result.o = A_.o + B_;
  Result.d = A_.d     ;

  return Result;
}

//------------------------------------------------------------------------------

TdFloat Sub( TdFloat A_, float B_ )
{
  TdFloat Result;

  Result.o = A_.o - B_;
  Result.d = A_.d     ;

  return Result;
}

TdFloat Sub( float A_, TdFloat B_ )
{
  TdFloat Result;

  Result.o = A_ - B_.o;
  Result.d =     -B_.d;

  return Result;
}

TdFloat Sub( TdFloat A_, TdFloat B_ )
{
  TdFloat Result;

  Result.o = A_.o - B_.o;
  Result.d = A_.d - B_.d;

  return Result;
}

//------------------------------------------------------------------------------

TdFloat Mul( TdFloat A_, TdFloat B_ )
{
  TdFloat Result;

  Result.o = A_.o * B_.o;
  Result.d = A_.d * B_.o + A_.o * B_.d;

  return Result;
}

TdFloat Mul( float A_, TdFloat B_ )
{
  TdFloat Result;

  Result.o = A_ * B_.o;
  Result.d = A_ * B_.d;

  return Result;
}

TdFloat Mul( TdFloat A_, float B_ )
{
  TdFloat Result;

  Result.o = A_.o * B_;
  Result.d = A_.d * B_;

  return Result;
}

//------------------------------------------------------------------------------

TdFloat Div( TdFloat A_, TdFloat B_ )
{
  TdFloat Result;

  Result.o =                 A_.o          /       B_.o  ;
  Result.d = ( A_.d * B_.o - A_.o * B_.d ) / Pow2( B_.o );

  return Result;
}

TdFloat Div( float A_, TdFloat B_ )
{
  TdFloat Result;

  Result.o =  A_        /       B_.o  ;
  Result.d = -A_ * B_.d / Pow2( B_.o );

  return Result;
}

TdFloat Div( TdFloat A_, float B_ )
{
  TdFloat Result;

  Result.o = A_.o / B_;
  Result.d = A_.d / B_;

  return Result;
}

////////////////////////////////////////////////////////////////////////////////

TdFloat Pow2( TdFloat A_ )
{
  TdFloat Result;

  Result.o = Pow2( A_.o );
  Result.d = 2 * A_.o * A_.d;

  return Result;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TdVec3

TdVec3 Add( TdVec3 A_, TdVec3 B_ )
{
  TdVec3 Result;

  Result.x = Add( A_.x, B_.x );
  Result.y = Add( A_.y, B_.y );
  Result.z = Add( A_.z, B_.z );

  return Result;
}

TdVec3 Add( vec3 A_, TdVec3 B_ )
{
  TdVec3 Result;

  Result.x = Add( A_.x, B_.x );
  Result.y = Add( A_.y, B_.y );
  Result.z = Add( A_.z, B_.z );

  return Result;
}

TdVec3 Add( TdVec3 A_, vec3 B_ )
{
  TdVec3 Result;

  Result.x = Add( A_.x, B_.x );
  Result.y = Add( A_.y, B_.y );
  Result.z = Add( A_.z, B_.z );

  return Result;
}

//------------------------------------------------------------------------------

TdVec3 Sub( TdVec3 A_, TdVec3 B_ )
{
  TdVec3 Result;

  Result.x = Sub( A_.x, B_.x );
  Result.y = Sub( A_.y, B_.y );
  Result.z = Sub( A_.z, B_.z );

  return Result;
}

TdVec3 Sub( vec3 A_, TdVec3 B_ )
{
  TdVec3 Result;

  Result.x = Sub( A_.x, B_.x );
  Result.y = Sub( A_.y, B_.y );
  Result.z = Sub( A_.z, B_.z );

  return Result;
}

TdVec3 Sub( TdVec3 A_, vec3 B_ )
{
  TdVec3 Result;

  Result.x = Sub( A_.x, B_.x );
  Result.y = Sub( A_.y, B_.y );
  Result.z = Sub( A_.z, B_.z );

  return Result;
}

//------------------------------------------------------------------------------

TdVec3 Mul( TdVec3 A_, TdVec3 B_ )
{
  TdVec3 Result;

  Result.x = Mul( A_.x, B_.x );
  Result.y = Mul( A_.y, B_.y );
  Result.z = Mul( A_.z, B_.z );

  return Result;
}

TdVec3 Mul( vec3 A_, TdVec3 B_ )
{
  TdVec3 Result;

  Result.x = Mul( A_.x, B_.x );
  Result.y = Mul( A_.y, B_.y );
  Result.z = Mul( A_.z, B_.z );

  return Result;
}

TdVec3 Mul( TdVec3 A_, vec3 B_ )
{
  TdVec3 Result;

  Result.x = Mul( A_.x, B_.x );
  Result.y = Mul( A_.y, B_.y );
  Result.z = Mul( A_.z, B_.z );

  return Result;
}

//------------------------------------------------------------------------------

TdVec3 Mul( TdFloat A_, TdVec3 B_ )
{
  TdVec3 Result;

  Result.x = Mul( A_, B_.x );
  Result.y = Mul( A_, B_.y );
  Result.z = Mul( A_, B_.z );

  return Result;
}

TdVec3 Mul( float A_, TdVec3 B_ )
{
  TdVec3 Result;

  Result.x = Mul( A_, B_.x );
  Result.y = Mul( A_, B_.y );
  Result.z = Mul( A_, B_.z );

  return Result;
}

TdVec3 Mul( TdFloat A_, vec3 B_ )
{
  TdVec3 Result;

  Result.x = Mul( A_, B_.x );
  Result.y = Mul( A_, B_.y );
  Result.z = Mul( A_, B_.z );

  return Result;
}

//------------------------------------------------------------------------------

TdVec3 Mul( TdVec3 A_, float B_ )
{
  TdVec3 Result;

  Result.x = Mul( A_.x, B_ );
  Result.y = Mul( A_.y, B_ );
  Result.z = Mul( A_.z, B_ );

  return Result;
}

TdVec3 Mul( vec3 A_, TdFloat B_ )
{
  TdVec3 Result;

  Result.x = Mul( A_.x, B_ );
  Result.y = Mul( A_.y, B_ );
  Result.z = Mul( A_.z, B_ );

  return Result;
}

TdVec3 Mul( TdVec3 A_, TdFloat B_ )
{
  TdVec3 Result;

  Result.x = Mul( A_.x, B_ );
  Result.y = Mul( A_.y, B_ );
  Result.z = Mul( A_.z, B_ );

  return Result;
}

//------------------------------------------------------------------------------

TdVec3 Div( TdVec3 A_, TdVec3 B_ )
{
  TdVec3 Result;

  Result.x = Div( A_.x, B_.x );
  Result.y = Div( A_.y, B_.y );
  Result.z = Div( A_.z, B_.z );

  return Result;
}

TdVec3 Div( vec3 A_, TdVec3 B_ )
{
  TdVec3 Result;

  Result.x = Div( A_.x, B_.x );
  Result.y = Div( A_.y, B_.y );
  Result.z = Div( A_.z, B_.z );

  return Result;
}

TdVec3 Div( TdVec3 A_, vec3 B_ )
{
  TdVec3 Result;

  Result.x = Div( A_.x, B_.x );
  Result.y = Div( A_.y, B_.y );
  Result.z = Div( A_.z, B_.z );

  return Result;
}

//------------------------------------------------------------------------------

TdVec3 Div( TdVec3 A_, TdFloat B_ )
{
  TdVec3 Result;

  Result.x = Div( A_.x, B_ );
  Result.y = Div( A_.y, B_ );
  Result.z = Div( A_.z, B_ );

  return Result;
}

TdVec3 Div( vec3 A_, TdFloat B_ )
{
  TdVec3 Result;

  Result.x = Div( A_.x, B_ );
  Result.y = Div( A_.y, B_ );
  Result.z = Div( A_.z, B_ );

  return Result;
}

TdVec3 Div( TdVec3 A_, float B_ )
{
  TdVec3 Result;

  Result.x = Div( A_.x, B_ );
  Result.y = Div( A_.y, B_ );
  Result.z = Div( A_.z, B_ );

  return Result;
}

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&（幾何学）

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% VecToSky

vec2 VecToSky( in vec3 Vec )
{
  vec2 Result;

  Result.x = ( Pi - atan( -Vec.x, -Vec.z ) ) / Pi2;
  Result.y =        acos(  Vec.y           ) / Pi ;

  return Result;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HitAABB

bool HitSlab( in  float RayP, in  float RayV,
              in  float MinP, in  float MaxP,
              out float MinT, out float MaxT )
{
  if ( RayV < -FLOAT_EPS2 )
  {
    MinT = ( MaxP - RayP ) / RayV;
    MaxT = ( MinP - RayP ) / RayV;
  }
  else
  if ( +FLOAT_EPS2 < RayV )
  {
    MinT = ( MinP - RayP ) / RayV;
    MaxT = ( MaxP - RayP ) / RayV;
  }
  else
  if ( ( MinP < RayP ) && ( RayP < MaxP ) )
  {
    MinT = -FLOAT_MAX;
    MaxT = +FLOAT_MAX;
  }
  else return false;

  return true;
}

//------------------------------------------------------------------------------

bool HitAABB( in  vec3  RayP, in  vec3  RayV,
              in  vec3  MinP, in  vec3  MaxP,
              out float MinT, out float MaxT,
              out int   IncA, out int   OutA )
{
  vec3 T0, T1;

  if ( HitSlab( RayP.x, RayV.x, MinP.x, MaxP.x, T0.x, T1.x )
    && HitSlab( RayP.y, RayV.y, MinP.y, MaxP.y, T0.y, T1.y )
    && HitSlab( RayP.z, RayV.z, MinP.z, MaxP.z, T0.z, T1.z ) )
  {
    IncA = MaxI( T0 );  MinT = T0[ IncA ];
    OutA = MinI( T1 );  MaxT = T1[ OutA ];

    return ( MinT < MaxT );
  }

  return false;
}

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&（光学）

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ToneMap

vec3 ToneMap( in vec3 Color, in float White )
{
  return clamp( Color * ( 1 + Color / White ) / ( 1 + Color ), 0, 1 );
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GammaCorrect

vec3 GammaCorrect( in vec3 Color, in float Gamma )
{
  vec3 Result;

  float G = 1 / Gamma;

  Result.r = pow( Color.r, G );
  Result.g = pow( Color.g, G );
  Result.b = pow( Color.b, G );

  return Result;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Fresnel

float Fresnel( in vec3 Vec, in vec3 Nor, in float IOR )
{
  float N2, C, G2, F0;
  // float N2C, G;

  N2 = Pow2( IOR );
  C  = dot( Nor, -Vec );
  G2 = N2 + Pow2( C ) - 1;
  if ( G2 < 0 ) return 1;

  // N2C = N2 * C;
  // G   = sqrt( G2 );
  // return ( Pow2( (   C - G ) / (   C + G ) )
  //        + Pow2( ( N2C - G ) / ( N2C + G ) ) ) / 2;

  F0 = Pow2( ( IOR - 1 ) / ( IOR + 1 ) );
  return F0 + ( 1 - F0 ) * pow( 1 - C, 5 );
}

//############################################################################## ■

layout( rgba32ui ) uniform uimage2D _Seeder;

layout( std430 ) buffer TAccumN
{
  int _AccumN;
};

layout( rgba32f ) uniform image2D _Accumr;

writeonly uniform image2D _Imager;

layout( std430 ) buffer TCamera
{
  layout( row_major ) mat4 _Camera;
};

uniform sampler2D _Textur;

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【型】

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TRay

struct TRay
{
  vec3 Pos;
  vec3 Vec;
  vec3 Wei;
  vec3 Emi;
};

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% THit

struct THit
{
  float t;
  int   Mat;
  vec3  Pos;
  vec3  Nor;
};

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【ルーチン】

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&（物体）

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ObjPlane

void ObjPlane( in TRay Ray, inout THit Hit )
{
  float t;

  if ( Ray.Vec.y < 0 )
  {
    t = ( Ray.Pos.y - -1.001 ) / -Ray.Vec.y;

    if ( ( 0 < t ) && ( t < Hit.t ) )
    {
      Hit.t   = t;
      Hit.Pos = Ray.Pos + t * Ray.Vec;
      Hit.Nor = vec3( 0, 1, 0 );
      Hit.Mat = 3;
    }
  }
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ObjSpher

void ObjSpher( in TRay Ray, inout THit Hit )
{
  float B, C, D, t;

  B = dot( Ray.Pos, Ray.Vec );
  C = length2( Ray.Pos ) - 1;

  D = Pow2( B ) - C;

  if ( D > 0 )
  {
    t = -B - sign( C ) * sqrt( D );

    if ( ( 0 < t ) && ( t < Hit.t ) )
    {
      Hit.t   = t;
      Hit.Pos = Ray.Pos + t * Ray.Vec;
      Hit.Nor = Hit.Pos;
      Hit.Mat = 2;
    }
  }
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ObjRecta

void ObjRecta( in TRay Ray, inout THit Hit )
{
  const vec3 MinP = vec3( -1, -1, -1 );
  const vec3 MaxP = vec3( +1, +1, +1 );

  float MinT, MaxT, T;
  int   IncA, OutA;

  if ( HitAABB( Ray.Pos, Ray.Vec, MinP, MaxP, MinT, MaxT, IncA, OutA ) )
  {
    if ( FLOAT_EPS2 < MinT ) T = MinT;
    else
    if ( FLOAT_EPS2 < MaxT ) T = MaxT;
    else return;

    if ( T < Hit.t )
    {
      Hit.t   = T;
      Hit.Pos = Ray.Pos + Hit.t * Ray.Vec;

      Hit.Nor = vec3( 0 );
      Hit.Nor[ IncA ] = -sign( Ray.Vec[ IncA ] );

      Hit.Mat = 2;
    }
  }
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ObjImpli

//------------------------------------------------------------------------------
const vec3  FieldPosMin = vec3( -5, -1, -5 );  //ハイトフィールドのサイズ
const vec3  FieldPosMax = vec3( +5, +1, +5 );
const uvec2 FieldDivN   = uvec2( 100, 100 );  //ハイトフィールドのピクセル数（分割数）
layout( rgba32f ) readonly uniform image2D _CLHeight;
uniform sampler2D _CLTex;

TdFloat Sqrt( TdFloat X )
{
  TdFloat Result;
  Result.o = sqrt( X.o );
  Result.d = X.d / ( 2 * sqrt( X.o ) );
  return Result;
}

TdFloat Atan(TdFloat X, TdFloat Y){
  TdFloat Result;
  Result.o = atan(X.o, Y.o);
  Result.d = (X.d/Y.d) / (1 + (X.o/Y.o) *(X.o/Y.o) );
  return Result;
}

TdFloat Acos(TdFloat X){
  TdFloat Result;
  Result.o = acos(X.o);
  Result.d = X.d / sqrt(1 - X.o* X.o);
  return Result;
}

TdFloat Length( TdVec3 V ){
  return Sqrt( Add(Add(Pow2(V.x) ,Pow2(V.y) ), Pow2(V.z)) );
}

TdFloat VecToSkyX( in TdVec3 Vec )
{
  TdFloat Result;
  Result = Div( Sub(Pi, Atan( Vec.x, Vec.z ) ) , Pi2);
  return Result;
}

TdFloat VecToSkyY( in TdVec3 Vec )
{
  TdFloat Result;
  Result = Div(Acos( Vec.y), Pi );
  return Result;
}

const float ActSphereR = 1.6f;
const float FakSphereR = 1.52f;
const int PixelsH = 20;
const int PixelsW = 20;

vec3 HomToPolar(in vec3 Pos){
  vec3 Result;

  Result.x = sqrt(Pos.x*Pos.x + Pos.y*Pos.y + Pos.z*Pos.z); // radius
  Result.y = asin(Pos.y / Result.x);//theta
  Result.z = atan(Pos.z , Pos.x);//phi

  return Result;
}

vec3 PolarToHom(in vec3 Pos){
  vec3 Result;

  Result.x = Pos.x * cos(Pos.y) * cos(Pos.z);
  Result.y = Pos.x * sin(Pos.y) * cos(Pos.z);
  Result.z = Pos.x * cos(Pos.y) * sin(Pos.z);

  return Result;
}

//Polar -> UV
vec2 PolarToUV(in vec3 Pos){
  vec2 Result;

  Result.x = (Pos.z + Pi ) / Pi2;
  Result.y = (Pos.y + P2i) / Pi; 

  return Result;
}

vec2 SphToPix(in vec2 Pos){
  vec2 Result;

  Result.x = Pos.x * PixelsH;
  Result.y = Pos.y * PixelsW;

  return Result;
}


bool HitField( in TRay Ray, inout THit Hit, out float MinT, out float MaxT){
  float B, C, Cf, D, Df;

  B = 2 * dot( Ray.Pos.xyz, Ray.Vec.xyz );
  C = length2( Ray.Pos.xyz ) - (ActSphereR+0.01)* (ActSphereR+0.01);
  Cf = length2( Ray.Pos.xyz ) - (FakSphereR-0.2) * (FakSphereR-0.2);
  //Cf = length2( Ray.Pos.xyz );

  D = Pow2( B ) - 4 * C;
  Df = Pow2( B ) - 4 * Cf;

  if ( D > 0 )
  {
    MinT = ( -B - sqrt( D ) ) / 2.0f;
    if(Df >= 0){
      MaxT = ( -B - sqrt( Df ) ) / 2.0f;
    } else {
      MaxT = ( -B + sqrt( D ) ) / 2.0f;
    }
    return true;
  }
  return false;
}

bool HitShell(in TRay Ray, inout THit Hit, out vec3 FrontSide, out vec3 BackSide,
  inout float MinT, inout float MaxT){
  float B, C, Cf, D, Df;

  B = 2 * dot( Ray.Pos.xyz, Ray.Vec.xyz );
  C = length2( Ray.Pos.xyz ) - (ActSphereR+0.01)* (ActSphereR+0.01);
  Cf = length2( Ray.Pos.xyz ) - (FakSphereR-0.2) * (FakSphereR-0.2);
  //Cf = length2( Ray.Pos.xyz );

  D = Pow2( B ) - 4 * C;
  Df = Pow2( B ) - 4 * Cf;

  if ( D > 0 )
  {
    MinT = ( -B - sqrt( D ) ) / 2.0f;
    FrontSide = Ray.Pos.xyz + Ray.Vec.xyz * MinT;
    if(Df >= 0){
      MaxT = ( -B - sqrt( Df ) ) / 2.0f;
      BackSide = Ray.Pos.xyz + Ray.Vec.xyz * MaxT;
    } else {
      MaxT = ( -B + sqrt( D ) ) / 2.0f;
      BackSide = Ray.Pos.xyz + Ray.Vec.xyz * MaxT;
    }
    return true;
  }
  return false;
}


TdFloat MathFunc( TdVec3 P )
{
  TdFloat F00, F01, F10, F11, F0, F1, F;
  float GX, GY, GXd, GYd;
  int     GXi, GYi;
  GX = VecToSky(normalize(vec3(P.x.o, P.y.o, P.z.o))).x * ( PixelsW );
  GY = VecToSky(normalize(vec3(P.x.o, P.y.o, P.z.o))).y * ( PixelsH );  
  GXi = int( floor( GX ) );  GXd = GX - GXi ;
  GYi = int( floor( GY ) );  GYd = GY - GYi ;
  //GXi = int( mod(floor( GX ) ,1024));  GXd = GX - GXi ;
  //GYi = int( mod(floor( GY ) ,1024));  GYd = GY - GYi ;
  F00 = TdFloat( imageLoad( _CLHeight, ivec2( GXi+0, GYi+0 ) ).r, 0 );  // 近傍の４ピクセルを取得
  F01 = TdFloat( imageLoad( _CLHeight, ivec2( GXi+1, GYi+0 ) ).r, 0 );
  F10 = TdFloat( imageLoad( _CLHeight, ivec2( GXi+0, GYi+1 ) ).r, 0 );
  F11 = TdFloat( imageLoad( _CLHeight, ivec2( GXi+1, GYi+1 ) ).r, 0 );
  F0 = Add( Mul( Sub( F01, F00 ), GXd ), F00 );
  F1 = Add( Mul( Sub( F11, F10 ), GXd ), F10 );
  F = Add( Mul( Sub( F1, F0 ), GYd ), F0 );
  return Sub( Length(P),  Add( Mul( ActSphereR - FakSphereR, F ), FakSphereR ) );
  //return Sub( P.y, Add( Mul( FieldPosMax.y- FieldPosMin.y, F ), FieldPosMin.y ) );  // 点Ｐとハイトフィールドとの距離
}

vec3 MathGrad( vec3 Pos )
{
  vec3   Result;
  TdVec3 P;

  P.x.o = Pos.x;
  P.y.o = Pos.y;
  P.z.o = Pos.z;

  P.x.d = 1;
  P.y.d = 0;
  P.z.d = 0;

  Result.x = MathFunc( P ).d;

  P.x.d = 0;
  P.y.d = 1;
  P.z.d = 0;

  Result.y = MathFunc( P ).d;

  P.x.d = 0;
  P.y.d = 0;
  P.z.d = 1;

  Result.z = MathFunc( P ).d;

  return Result;
}

//------------------------------------------------------------------------------

bool HitFunc( in TRay Ray, in float T2d, inout float HitT, out vec3 HitP ){
  const uint LoopN = 16;

  float   Tds, Td;
  uint    N;
  TdFloat T;
  TdVec3  P;
  TdFloat F;

  Tds = 0;

  T = TdFloat( HitT, 1 );

  for ( N = 1; N <= LoopN; N++ )
  {
    P = Add( Mul( Ray.Vec, T ), Ray.Pos );

    F = MathFunc( P );

    if ( abs( F.o ) < 0.001 )
    {
      HitT   = T.o;

      HitP.x = P.x.o;
      HitP.y = P.y.o;
      HitP.z = P.z.o;

      return true;
    }

    Td = -F.o / F.d;

    Tds += Td;

    if ( abs( Tds ) > T2d ) return false;

    T.o += Td;
  }

  return false;
}



void ObjImpli( in TRay Ray, inout THit Hit ){
  //const vec3  MinP = FieldPosMin + FLOAT_EPS2;
  //const vec3  MaxP = FieldPosMax - FLOAT_EPS2;
  const float Td = ( ActSphereR - FakSphereR ) / 100;
  const float T2d  = 1.5 * Td/2;
  float MinT, MaxT, T0;
  //int   IncA, OutA;
  float T;
  vec3  P;
  if ( HitField(Ray, Hit, MinT, MaxT ))//HitAABB( Ray.Pos, Ray.Vec, MinP, MaxP, MinT, MaxT, IncA, OutA ) )
  {
    for ( T0 = max( 0, MinT ); T0 < MaxT + Td; T0 += Td )
    {
      T = T0;
      if ( HitFunc( Ray, T2d, T, P ) && ( 0 < T ) && ( T < MaxT ) && ( T < Hit.t ) )
      {
        Hit.t   = T;
        Hit.Pos = P;
        Hit.Nor = normalize( MathGrad( P ) );
        Hit.Mat = 3;
        break;
      }
    }
  }
}

bool Hit_AABB( in  vec3  RayP, in  vec3  RayV,
              in  vec3  MinP, in  vec3  MaxP)
{
  vec3 T0, T1;
  int IncA, OutA;
  float MinT, MaxT;

  if ( HitSlab( RayP.x, RayV.x, MinP.x, MaxP.x, T0.x, T1.x )
    && HitSlab( RayP.y, RayV.y, MinP.y, MaxP.y, T0.y, T1.y )
    && HitSlab( RayP.z, RayV.z, MinP.z, MaxP.z, T0.z, T1.z ) )
  {
    IncA = MaxI( T0 );  MinT = T0[ IncA ];
    OutA = MinI( T1 );  MaxT = T1[ OutA ];

    return ( MinT < MaxT );
  }

  return false;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ObjBiPat
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% QuadSolver
uint QuadSolver( in float A, in float B, in float C,
                      out float X1, out float X2 )
{
  float D2, D, A2, C2, BD;
  if ( A == 0 )
  {
    X1 = -C / B;
    return 1;
  }
  A2 = 2 * A;
  C2 = 2 * C;
  D2 = Pow2( B ) - A2 * C2;
  if ( D2 == 0 )
  {
    X1 = -B / A2; //= C2 / -B
    return 1;
  }
  if ( 0 < D2 )
  {
    D = sqrt( D2 );
    if ( B >= 0 )
    {
      BD = -B - D;
      X1 = BD / A2;
      X2 = C2 / BD;
    }
    else
    {
      BD = -B + D;
      X1 = C2 / BD;
      X2 = BD / A2;
    }
    return 2;
  }
  return 0;
}

struct TBiPatch
{
  vec3 P00, P01,
       P10, P11;
};

struct TBiPatchPara
{
  vec3 a, b, c, d;
};

struct TBiPatchSolv
{
  vec2 A, B, C, D;
};
float CalcCX( in TBiPatchSolv PatS, in float CY )  // Ｖ座標からＵ座標を計算
{
  float U1, U2;
  U1 = CY *   PatS.A.y              +   PatS.B.y             ;
  U2 = CY * ( PatS.A.y - PatS.A.x ) + ( PatS.B.y - PatS.B.x );
  if ( abs( U2 ) >= abs( U1 ) )
    return ( CY * ( PatS.C.x - PatS.C.y ) + ( PatS.D.x - PatS.D.y ) ) / U2;
  else
    return ( CY *            - PatS.C.y   +            - PatS.D.y )   / U1;
}
uint HitBiPatch( in TBiPatchPara PatK,
                 in vec3 RayP, in vec3 RayV,
                 out vec2 HitC0, out vec2 HitC1 )  // レイとの交点をＵＶ座標で返す関数
{
  TBiPatchSolv PatS;
  float        A, B, C, D2, D, A2;
  switch( MaxI( abs( RayV ) ) )  // 論文にはないが、レイの方向によって計算を変えた方が精度が上がった。
  {
    case 0:
        PatS.A =   PatK.a.yz             * RayV.x -   PatK.a.x            * RayV.yz;
        PatS.B =   PatK.b.yz             * RayV.x -   PatK.b.x            * RayV.yz;
        PatS.C =   PatK.c.yz             * RayV.x -   PatK.c.x            * RayV.yz;
        PatS.D = ( PatK.d.yz - RayP.yz ) * RayV.x - ( PatK.d.x - RayP.x ) * RayV.yz;
      break;
    case 1:
        PatS.A =   PatK.a.zx             * RayV.y -   PatK.a.y            * RayV.zx;
        PatS.B =   PatK.b.zx             * RayV.y -   PatK.b.y            * RayV.zx;
        PatS.C =   PatK.c.zx             * RayV.y -   PatK.c.y            * RayV.zx;
        PatS.D = ( PatK.d.zx - RayP.zx ) * RayV.y - ( PatK.d.y - RayP.y ) * RayV.zx;
      break;
    case 2:
        PatS.A =   PatK.a.xy             * RayV.z -   PatK.a.z            * RayV.xy;
        PatS.B =   PatK.b.xy             * RayV.z -   PatK.b.z            * RayV.xy;
        PatS.C =   PatK.c.xy             * RayV.z -   PatK.c.z            * RayV.xy;
        PatS.D = ( PatK.d.xy - RayP.xy ) * RayV.z - ( PatK.d.z - RayP.z ) * RayV.xy;
      break;
  }
  A = PatS.A.y * PatS.C.x - PatS.A.x * PatS.C.y;  // 二次方程式の係数
  B = PatS.A.y * PatS.D.x - PatS.A.x * PatS.D.y   //
    + PatS.B.y * PatS.C.x - PatS.B.x * PatS.C.y;  //
  C = PatS.B.y * PatS.D.x - PatS.B.x * PatS.D.y;  //
  switch ( QuadSolver( A, B, C, HitC0.y, HitC1.y ) )  // 二次方程式の解の数によって分岐
  {
    case 0:
      return 0;
    case 1:
        HitC0.x = CalcCX( PatS, HitC0.y );
      return 1;
    case 2:
        HitC0.x = CalcCX( PatS, HitC0.y );
        HitC1.x = CalcCX( PatS, HitC1.y );
      return 2;
  }
}

vec3 BiPatchPos( in TBiPatchPara PatK, in vec2 PatC )  // ＵＶ座標から３Ｄ座標を計算
{
  return PatK.a * PatC.x * PatC.y
       + PatK.b * PatC.x
       + PatK.c          * PatC.y
       + PatK.d;
}
mat3 BiPatchRot( in TBiPatchPara PatK, in vec2 PatC )  // ＵＶ座標からローカル座標系の回転行列を計算
{
  vec3 T, B, N;
  T = normalize( PatK.a * PatC.y + PatK.b );  // 接線
  B = normalize( PatK.a * PatC.x + PatK.c );  // 従法線
  N = cross( T, B );                          // 方瀬名
  return mat3( T, B, N );  // ３軸を行列にまとめる
}

void CalcHit( in TBiPatchPara PatK, in vec2 PatC, in TRay Ray, inout THit Hit ,
  vec3 Start, vec3 Vec, float MinT, float MaxT)
{
  vec3  P;
  float T;
  if ( ( 0 <= PatC.x ) && ( PatC.x <= 1 )    //  交点のＵＶ座標が０～１の範囲ならヒット
    && ( 0 <= PatC.y ) && ( PatC.y <= 1 ) )  //
  {
    P = BiPatchPos( PatK, PatC );     // 交点のＵＶ座標から３Ｄ座標を逆算
    T = dot( P - Start, Vec );  // 交点の３Ｄ座標からレイの長さを逆算
    T = (MaxT-MinT)*T + MinT ;
    if ( ( 0 < T ) && ( T < Hit.t ) )  // レイの長さが０以上で他の物体（Hit.t）より近かったらヒット
    {
      Hit.t   = T;
      Hit.Pos = Ray.Pos + Ray.Vec * T;
      vec3 NorVec = normalize(P);
      vec3 TanVec = normalize(cross(vec3(0,1,0), NorVec));
      vec3 BinVec = cross(NorVec, TanVec);
      Hit.Nor = mat3(TanVec, BinVec, NorVec) * BiPatchRot( PatK, PatC )[ 2 ];
      //Hit.Nor = BiPatchRot( PatK, PatC )[ 2 ];  // 交点のＵＶ座標からローカル座標の回転行列を計算し法線ベクトルのみを抜き出す
      Hit.Mat = 1;
    }
  }
}

void ObjBiPat( in TRay Ray, inout THit Hit , 
  vec3 first, vec3 second, vec3 third, vec3 fourth,
  vec3 Start, vec3 End, float MinT, float MaxT)
{
  TBiPatch     PatG;
  TBiPatchPara PatK;
  vec2         C0, C1;
  vec3         P;
  float        T;
  vec3 Vec = normalize(End - Start);
  PatG.P00 = first; PatG.P01 = second;
  PatG.P10 = third; PatG.P11 = fourth;
  //PatG.P00 = vec3( -1, +1, -1 );  PatG.P01 = vec3( +1, -1, -1 );  // ４頂点の３Ｄ座標
  //PatG.P10 = vec3( -1, -1, +1 );  PatG.P11 = vec3( +1, +1, +1 );  //
  PatK.a = PatG.P11 - PatG.P10 - PatG.P01 + PatG.P00;  //多項式表現における係数を計算
  PatK.b =            PatG.P10            - PatG.P00;  //
  PatK.c =                       PatG.P01 - PatG.P00;  //
  PatK.d =                                  PatG.P00;  //
  switch ( HitBiPatch( PatK, Start, Vec, C0, C1 ) )  // レイとの交点数によって分岐
  {
    case 1:
        CalcHit( PatK, C0, Ray, Hit , Start, Vec, MinT, MaxT);
      break;
    case 2:
        CalcHit( PatK, C0, Ray, Hit , Start, Vec, MinT, MaxT);
        CalcHit( PatK, C1, Ray, Hit , Start, Vec, MinT, MaxT);
      break;
  }
}

///////////////////////////////////////////////////////////////////////

void HitFieldFace( in TRay Ray, inout THit Hit, vec3 Start, vec3 End, vec2 PixPos,
    float avg, float MinT, float MaxT){
  vec3 Vec = End - Start;
  float t = (avg - Start.z)/Vec.z;
  t = (MaxT-MinT)*t + MinT;

  if( ( 0 < t ) && ( t < Hit.t ) ){
    Hit.t = (MaxT-MinT)*t + MinT;
    Hit.Pos = Ray.Pos.xyz + Hit.t * Ray.Vec.xyz;
    Hit.Nor = normalize(Hit.Pos);
    Hit.Mat = 1;
  }
}

void HitFace(in TRay Ray, inout THit Hit, vec2 PixPos, vec3 Start, vec3 End, float MinT, float MaxT){
  float F00, F01, F10, F11, avg;
  F00 = imageLoad( _CLHeight, ivec2( mod(PixPos.x+0,1024), mod(PixPos.y+0,1024)) ).r;
  F01 = imageLoad( _CLHeight, ivec2( mod(PixPos.x+1,1024), mod(PixPos.y+0,1024)) ).r;
  F10 = imageLoad( _CLHeight, ivec2( mod(PixPos.x+0,1024), mod(PixPos.y+1,1024)) ).r;
  F11 = imageLoad( _CLHeight, ivec2( mod(PixPos.x+1,1024), mod(PixPos.y+1,1024)) ).r;
  avg = (F00+F01+F10+F11)/4;
  
  /*
  const vec3 MinP = vec3( PixPos , 0 );
  const vec3 MaxP = vec3( PixPos + vec2(1), 1 );
  float MinTt, MaxTt, T;
  int   IncA, OutA;
  if ( HitAABB( Ray.Pos, Ray.Vec, MinP, MaxP, MinTt, MaxTt, IncA, OutA ) )
  {
    if ( FLOAT_EPS2 < MinTt ) T = MinTt;
    else
    if ( FLOAT_EPS2 < MaxTt ) T = MaxTt;
    else return;
    T=(MaxT-MinT)*T + MinT;
    if ( T < Hit.t )
    {
      Hit.t   = T;
      Hit.Pos = Ray.Pos + Hit.t * Ray.Vec;
      Hit.Nor = vec3( 0 );
      Hit.Nor[ IncA ] = -sign( Ray.Vec[ IncA ] );
      Hit.Mat = 1;
    }
  }*/

  
  HitFieldFace(Ray, Hit, Start, End, PixPos, avg, MinT, MaxT);

  //ObjBiPat(Ray, Hit, vec3( PixPos.x, PixPos.y, F00 ), vec3(PixPos.x + 1, PixPos.y, F01),
  // vec3(PixPos.x, PixPos.y + 1, F10), vec3(PixPos.x + 1, PixPos.y + 1, F11) ,
  // Start, End, MinT, MaxT);
}

void DDA2D(in TRay Ray, inout THit Hit, vec2 Start, vec2 End, float MinT, float MaxT){
  float Dx, Dy, Tx, Ty;
  vec2 V = End - Start; V = normalize(V);
  vec3 VoxStart = vec3(Start, 1);
  vec3 VoxVec   = normalize(vec3(End-Start, -1));
  Dx = 1 / abs(V.x); Dy = 1 / abs(V.y);
  if(V.x > 0){
    Tx = Dx * (1 - Start.x + floor(Start.x));
  } else {
    Tx = Dx * (Start.x - floor(Start.x) );
  }
  if(V.y > 0){
    Ty = Dy * (1 - Start.y + floor(Start.y));
  } else {
    Ty = Dy * (Start.y - floor(Start.y) );
  }
  HitFace(Ray,Hit,vec2(floor(Start.x), floor(Start.y)), VoxStart, VoxVec, MinT, MaxT);
  for(;Start.x + Tx < End.x || Start.y + Ty < End.y;){
    if(Tx < Ty){    
      HitFace(Ray, Hit, vec2(floor(Start.x + Tx), floor(Start.y + Ty-Dy)) , VoxStart, VoxVec, MinT, MaxT);
      Tx = Tx + Dx;
    } else {
      HitFace(Ray, Hit, vec2(floor(Start.x + Tx-Dx), floor(Start.y + Ty)) , VoxStart, VoxVec, MinT, MaxT);
      Ty = Ty + Dy;
    }
  }
}

void ObjYarn(in TRay Ray, inout THit Hit){
  vec3 FrontSide, BackSide;//Hom Coord
  vec2 InSph, OutSph;//Sph Coord
  vec2 InPix, OutPix;//Pixel Coord
  float MinT, MaxT;
  if(HitShell(Ray, Hit, FrontSide, BackSide, MinT, MaxT)){
    InSph  = VecToSky(normalize(FrontSide)); OutSph = VecToSky(normalize(BackSide));
    InPix  = SphToPix(InSph); OutPix = SphToPix(OutSph);
    DDA2D(Ray, Hit, InPix, OutPix, MinT, MaxT);
  }
}
//------------------------------------------------------------------------------


//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&（材質）

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% MatSkyer

bool MatSkyer( inout TRay Ray, in THit Hit )
{
  Ray.Emi += texture( _Textur, VecToSky( Ray.Vec ) ).rgb;

  return false;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% MatMirro

bool MatMirro( inout TRay Ray, in THit Hit )
{
  Ray.Pos = Hit.Pos + FLOAT_EPS2 * Hit.Nor;
  Ray.Vec = reflect( Ray.Vec, Hit.Nor );

  return true;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% MatWater

bool MatWater( inout TRay Ray, in THit Hit )
{
  TRay  Result;
  float C, IOR, F;
  vec3  Nor;

  C = dot( Hit.Nor, -Ray.Vec );

  if( 0 < C )
  {
    IOR = 1.333 / 1.000;
    Nor = +Hit.Nor;
  }
  else
  {
    IOR = 1.000 / 1.333;
    Nor = -Hit.Nor;
  }

  F = Fresnel( Ray.Vec, Nor, IOR );

  if ( Rand() < F )
  {
    Ray.Pos = Hit.Pos + FLOAT_EPS2 * Nor;
    Ray.Vec = reflect( Ray.Vec, Nor );
  } else {
    Ray.Pos = Hit.Pos - FLOAT_EPS2 * Nor;
    Ray.Vec = refract( Ray.Vec, Nor, 1 / IOR );
  }

  return true;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% MatDiffu

bool MatDiffu( inout TRay Ray, in THit Hit )
{
  vec3  AX, AY, AZ, DX, DY, V;
  mat3  M;
  float R, T;

  AZ = Hit.Nor;

  switch( MinI( abs( AZ ) ) )
  {
    case 0:
        DX = vec3( 1, 0, 0 );
        AY = normalize( cross( AZ, DX ) );
        AX = cross( AY, AZ );
      break;
    case 1:
        DY = vec3( 0, 1, 0 );
        AX = normalize( cross( DY, AZ ) );
        AY = cross( AZ, AX );
      break;
    case 2:
        DX = vec3( 0, 0, 1 );
        AY = normalize( cross( AZ, DX ) );
        AX = cross( AY, AZ );
      break;
  }

  M = mat3( AX, AY, AZ );

  V.z = sqrt( Rand() );

  R = sqrt( 1 - Pow2( V.z ) );
  T = Rand();

  V.x = R * cos( Pi2 * T );
  V.y = R * sin( Pi2 * T );

  Ray.Pos = Hit.Pos + FLOAT_EPS2 * Hit.Nor;
  Ray.Vec = M * V;

  Ray.Wei = Ray.Wei * texture( _CLTex, mod(VecToSky(normalize(Hit.Nor))*2,1) ).rgb;

  return true;
}

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

void Raytrace( inout TRay Ray )
{
  int  L;
  THit Hit;

  for ( L = 1; L <= 8; L++ )
  {
    Hit = THit( FLOAT_MAX, 0, vec3( 0 ), vec3( 0 ) );

    ///// 物体

    //ObjPlane( Ray, Hit );
    //ObjSpher( Ray, Hit );
    //ObjRecta( Ray, Hit );
    //ObjImpli( Ray, Hit );
    ObjYarn(Ray, Hit);
    //ObjBiPat(Ray,Hit, vec3(0));

    ///// 材質

    switch( Hit.Mat )
    {
      case 0: if ( MatSkyer( Ray, Hit ) ) break; else return;
      case 1: if ( MatMirro( Ray, Hit ) ) break; else return;
      case 2: if ( MatWater( Ray, Hit ) ) break; else return;
      case 3: if ( MatDiffu( Ray, Hit ) ) break; else return;
    }
  }
}

//------------------------------------------------------------------------------

void main()
{
  uint N;
  vec3 E, S;
  TRay R;
  vec3 A, C, P;

  _RandSeed = imageLoad( _Seeder, _WorkID.xy );

  if ( _AccumN == 0 ) A = vec3( 0 );
                 else A = imageLoad( _Accumr, _WorkID.xy ).rgb;

  for( N = _AccumN+1; N <= _AccumN+16; N++ )
  {
    E = vec3( 0.02 * RandCirc(), 0 );

    S.x = 4.0 * (       ( _WorkID.x + 0.5 + RandBS4() ) / _WorksN.x - 0.5 );
    S.y = 3.0 * ( 0.5 - ( _WorkID.y + 0.5 + RandBS4() ) / _WorksN.y       );
    S.z = -2;

    R.Pos = vec3( _Camera * vec4(                E  , 1 ) );
    R.Vec = vec3( _Camera * vec4( normalize( S - E ), 0 ) );
    R.Wei = vec3( 1 );
    R.Emi = vec3( 0 );

    Raytrace( R );

    C = R.Wei * R.Emi;

    A += ( C - A ) / N;
  }

  imageStore( _Accumr, _WorkID.xy, vec4( A, 1 ) );

  P = GammaCorrect( ToneMap( A, 10 ), 2.2 );

  imageStore( _Imager, _WorkID.xy, vec4( P, 1 ) );

  imageStore( _Seeder, _WorkID.xy, _RandSeed );
}

//############################################################################## ■
